import org.junit.Assert;
import org.junit.Test;

public class BubbleSortImplTest {

    private static final BubbleSort BUBBLE_SORT = new BubbleSortImpl();

    @Test(expected = IllegalArgumentException.class)
    public void testSort_null_input(){
        BUBBLE_SORT.sort(null);
    }

    @Test
    public void testSort_empty_input(){
        final int[] values = {};
        BUBBLE_SORT.sort(values);
        Assert.assertEquals("Array not empty.", 0, values.length);
    }

    @Test
    public void testSort_one_item(){
        final int[] values = {8};
        BUBBLE_SORT.sort(values);
        Assert.assertEquals("Array size changed.", 1, values.length);
        Assert.assertEquals("Array item not found.", 8, values[0]);
    }

    @Test
    public void testSort_two_items(){
        final int[] values = {8, 9};
        BUBBLE_SORT.sort(values);
        Assert.assertEquals("Array size changed.", 2, values.length);
        Assert.assertEquals("Array item not found or not sorted correctly.", 8, values[0]);
        Assert.assertEquals("Array item not found or not sorted correctly.", 9, values[1]);
    }


    @Test
    public void testSort_two_items_negative(){
        final int[] values = {8, -9};
        BUBBLE_SORT.sort(values);
        Assert.assertEquals("Array size changed.", 2, values.length);
        Assert.assertEquals("Array item not found or not sorted correctly.", -9, values[0]);
        Assert.assertEquals("Array item not found or not sorted correctly.", 8, values[1]);
    }


    @Test
    public void testSort_more_items(){
        final int[] values = {6,0,6,5,2,8};
        BUBBLE_SORT.sort(values);
        Assert.assertEquals("Array size changed.", 6, values.length);
        Assert.assertEquals("Array item not found or not sorted correctly.", 0, values[0]);
        Assert.assertEquals("Array item not found or not sorted correctly.", 2, values[1]);
        Assert.assertEquals("Array item not found or not sorted correctly.", 5, values[2]);
        Assert.assertEquals("Array item not found or not sorted correctly.", 6, values[3]);
        Assert.assertEquals("Array item not found or not sorted correctly.", 6, values[4]);
        Assert.assertEquals("Array item not found or not sorted correctly.", 8, values[5]);
    }


    @Test
    public void testSort_more_items_2(){
        final int[] values = {0,5,0,4,6,2};
        BUBBLE_SORT.sort(values);
        Assert.assertEquals("Array size changed.", 6, values.length);
        Assert.assertEquals("Array item not found or not sorted correctly.", 0, values[0]);
        Assert.assertEquals("Array item not found or not sorted correctly.", 0, values[1]);
        Assert.assertEquals("Array item not found or not sorted correctly.", 2, values[2]);
        Assert.assertEquals("Array item not found or not sorted correctly.", 4, values[3]);
        Assert.assertEquals("Array item not found or not sorted correctly.", 5, values[4]);
        Assert.assertEquals("Array item not found or not sorted correctly.", 6, values[5]);
    }


    @Test
    public void testSort_already_sorted_items(){
        final int[] values = {1,2,2,3};
        BUBBLE_SORT.sort(values);
        Assert.assertEquals("Array size changed.", 4, values.length);
        Assert.assertEquals("Array item not found or not sorted correctly.", 1, values[0]);
        Assert.assertEquals("Array item not found or not sorted correctly.", 2, values[1]);
        Assert.assertEquals("Array item not found or not sorted correctly.", 2, values[2]);
        Assert.assertEquals("Array item not found or not sorted correctly.", 3, values[3]);
    }

}
