import java.util.Comparator;

/** Bubble sort algorithm. */
public interface BubbleSort {

    /**
     * Sorts the input array in ascenting order using bubble sort algorithm.
     * For the sake of this interview avoid using existing implementations and libraries such as {@link java.util.Collections}.
     * @param values In/out integer array. Not null.
     * @throws IllegalArgumentException if input array is null.
     */
    void sort(final int[] values);

    /**
     * Sorts the input array in ascenting order using bubble sort algorithm.
     * For the sake of this interview avoid using existing implementations and libraries such as {@link java.util.Collections}.
     * @param values In/out integer array. Not null.
     * @param comparator Comparator to be used for sorting. Not null.
     * @throws IllegalArgumentException if input array is null or comparator is null.
     */
    void sortWithComparator(final int[] values, final Comparator<Integer> comparator);

}
